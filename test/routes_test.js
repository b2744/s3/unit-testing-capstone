const chai = require('chai');

const expect = chai.expect;

const http = require('chai-http');

chai.use(http);

describe('API Test ExchangeRates', ()=>{

	it('Test POST ExchangeRates is running', ()=>{
		chai.request('http://localhost:5001').post('/currency').end((err,res)=>{
			expect(res).to.not.equal(undefined)
		})
	})
	it('Test post ExchangeRates returns 400 if NAME is missing',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 'money',
			ex : {
				'peso': 50,
        		'won': 118,
        		'yen': 108,
        		'yuan': 3
			}
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if NAME is not a string',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 'money',
			name : 123,
			ex : {
				'peso': 50,
        		'won': 118,
        		'yen': 108,
        		'yuan': 7
			}
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if NAME is empty string',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 'money',
			name : undefined ,
			ex : {
				'peso': 50,
        		'won': 118,
        		'yen': 108,
        		'yuan': 7
			}
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if EX is missing',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 'money',
			name : 'Dollar'
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if EX is not object',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 'money',
			name : 'Dollar',
			ex : "1"
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if EX is an empty object',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 'money',
			name : 'Dollar',
			ex : undefined
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if ALIAS is missing',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			name : 'Dollar',
			ex : {
				'peso': 50,
        		'won': 118,
        		'yen': 108,
        		'yuan': 3
			}
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if ALIAS is not a string',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 123,
			name : "Dollar",
			ex : {
				'peso': 50,
        		'won': 118,
        		'yen': 108,
        		'yuan': 7
			}
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if ALIAS is empty string',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : undefined,
			name : "Dollar " ,
			ex : {
				'peso': 50,
        		'won': 118,
        		'yen': 108,
        		'yuan': 7
			}
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 400 if ALIAS has duplicate',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 'dollars',
			name : "Dollar " ,
			ex : {
				'peso': 50,
        		'won': 118,
        		'yen': 108,
        		'yuan': 7
			}
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400)
			done();
		})
	})
	it('Test post ExchangeRates returns 200 if ALIAS has no duplicate',(done)=>{
		chai.request('http://localhost:5001').post('/currency').type('json')
		.send({
			alias : 'dollars1',
			name : "Dollar " ,
			ex : {
				'peso': 50,
        		'won': 118,
        		'yen': 108,
        		'yuan': 7
			}
		})
		.end((err,res)=>{
			expect(res.status).to.equal(200)
			done();
		})
	})


})