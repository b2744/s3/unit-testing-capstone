const { exchangeRates } = require('../src/util.js')

module.exports = (app) => {

	app.get('/',(req,res)=>{
		return res.send({data:{}})
	});

	app.post('/currency', (req,res)=>{
		
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter NAME'
			})
		}
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : NAME has to be a string'
			})
		}
		if(req.body.name === undefined){
			return res.status(400).send({
				'error' : 'Bad Request : NAME must not be empty'
			})
		}
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter EX'
			})
		}
		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error' : 'Bad Request : EX has to be a object'
			})
		}
		if(req.body.ex === undefined){
			return res.status(400).send({
				'error' : 'Bad Request : EX must not be empty'
			})
		}
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter ALIAS'
			})
		}
		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS has to be a string'
			})
		}
		if(req.body.alias === undefined){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS must not be empty'
			})
		}
		if(req.body.alias === exchangeRates.usd.alias || req.body.alias === exchangeRates.yen.alias || req.body.alias === exchangeRates.peso.alias || req.body.alias === exchangeRates.yuan.alias || req.body.alias === exchangeRates.won.alias ){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS has duplicate'
			})
		}
		if(req.body.alias !== exchangeRates.usd.alias || req.body.alias !== exchangeRates.yen.alias || req.body.alias !== exchangeRates.peso.alias || req.body.alias !== exchangeRates.yuan.alias || req.body.alias !== exchangeRates.won.alias ){
			return res.status(200).send({
				'error' : 'Bad Request : ALIAS has no duplicate'
			})
		}


	});



}